﻿// DZ_19_5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class Animal
{
public:
    Animal(){}


    virtual void Voice()
    {

    }
};

class Dog : public Animal
{
public:
    Dog() {}


    void Voice() override
    {
        std::cout << "woof!\n";
    }
};

class Cat : public Animal
{
public:
    Cat() {}


    void Voice() override
    {
        std::cout << "meow!\n";
    }
};

class Sheep : public Animal
{
public:
    Sheep() {}


    void Voice() override
    {
        std::cout << "mee!\n";
    }
};

int main()
{
    const int Size = 3;
    Dog* myDog = new Dog;
    Cat* myCat = new Cat;
    Sheep* mySheep = new Sheep;

    Animal* Animals[Size]{ myDog, myCat, mySheep };


    for(int i = 0; i<Size; i++)
    {

        Animals[i]->Voice();
    
    }
    delete myDog;
    delete myCat;
    delete mySheep;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
